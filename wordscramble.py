#! python3
import sys, os

ansi_escape_color = '\033'
colors = {
    'red'       : '[1;31m',
    'blue'      : '[1;34m',
    'cyan'      : '[1;36m',
    'green'     : '[1;32m',
    'reset'     : '[0;0m',
    'bold'      : '[;1m',
    'underline' : '[4m',
    'reverse'   : '[;7m',
}

def get_matrix(file_path) -> list:
    '''Reads contignious lines of letters from a text file and 
    returns a 2D array of the letter matrix.

    :param str file_path: The path to the text file.

    :returns: 2D array of chars.
    :rtype: list[list[str]]
    '''
    with open(file_path, 'r') as file:
        matrix = [list(row.strip()) for row in file.readlines()]
    return matrix


def line_search(word, matrix, point, vector) -> list:
    '''Recursively tests if there is a contiguous line of letters of a word
    in a char matrix. 
    :param str word: The word to find in the matrix.
    :param list[list[str]] matrix: The matrix of letters.
    :param (int, int) point: The position (row, col) in the matrix to check for letter.
    :param (int, int) vector: The direction of the search.

    :returns: List of points in matrix which match letters in word.
    :rtype: list[(int, int)]
    '''
    if (point[0] < 0 or point[0] >= len(matrix) or 
        point[1] < 0 or point[1] >= len(matrix[0]) or
        vector == (0, 0)):
        return []
    if len(word) <= 1:
        return ([(point[0], point[1])] if 
            matrix[point[0]][point[1]] == word[0] else [])

    if matrix[point[0]][point[1]] == word[0]:
        next_ = line_search(word[1:], matrix, 
            (point[0] + vector[0], point[1] + vector[1]), vector)
        return [(point[0], point[1])] + next_

    return []


def star_search(word, matrix, point) -> list:
    '''Searches for a word in a matrix by testing in all directions from the first letter in the word.
    :param str word: The word to find in the matrix.
    :param list[list[str]] matrix: The matrix of letters.
    :param (int, int) point: The position (row, col) in the matrix of the first letter in the word.
    
    :returns: All list of points in matrix which match letters in word and starts at point.
    :rtype: list[list[(int, int)]]
    '''
    found_words = []
    for i in range(-1, 2, 1):
        for j in range(-1, 2, 1):
            word_coords = line_search(word, matrix, point, (i, j))
            if len(word_coords) == len(word):
                found_words.append(word_coords)
    return found_words


def word_in_matrix(word, matrix) -> list:
    '''Search the matrix for all occurences of the word.
    :param str word: The word to find.
    :param list[list[str]] matrix: A 2D array of letters.

    :returns: List of all occurences of the word, represented by lists of points of the letters in the matrix.
    :rtype: list[list[(int, int)]]
    '''
    found_words = []
    for row in range(len(matrix)):
        for col in range(len(matrix[0])):
            if matrix[row][col] == word[0]:
                found_words.extend(star_search(word, matrix, (row, col)))
                
    return found_words


def main():
    '''Print matrix to stdout, wait for input and try to find word in matrix.'''
    matrix = get_matrix('wordscramble_matrix.txt')

    found_words = []
    while True:
        #os.system('cls' if os.name == 'nt' else 'clear')
        os.system('')
        suffix = ansi_escape_color + colors['reset']
        for row in range(len(matrix)):
            for col in range(len(matrix[0])):
                prefix = ' '
                char = matrix[row][col]
                for position in found_words:
                    if (row, col) in position:
                        prefix += ansi_escape_color + colors['blue']
                print(prefix + char + suffix, end='')
            print()

        print()
        word = input('Enter the word to find (or exit to end): ')
        print()
        if word != False and word != '':
            if word.casefold() == 'exit':
                sys.exit()
            found_words = word_in_matrix(word.upper(), matrix)


if __name__ == '__main__':
    main()
